// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "T4TShooterGameMode.generated.h"

UCLASS(minimalapi)
class AT4TShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AT4TShooterGameMode();
};



