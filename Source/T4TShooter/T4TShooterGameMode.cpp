// Copyright Epic Games, Inc. All Rights Reserved.

#include "T4TShooterGameMode.h"
#include "T4TShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

AT4TShooterGameMode::AT4TShooterGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
