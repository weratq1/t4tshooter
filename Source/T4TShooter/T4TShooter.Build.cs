// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class T4TShooter : ModuleRules
{
	public T4TShooter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput" });
		PublicIncludePaths.AddRange(
			new string[] {
				"T4TShooter"
			}
		);
		
	}
}
