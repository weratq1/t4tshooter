// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "T4TWeaponBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWeaponEvent);

UENUM(BlueprintType)
enum class EWeaponType: uint8
{
	Pistol,
	AssaultRifle
	
};

UCLASS()
class T4TSHOOTER_API AT4TWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AT4TWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Server, Reliable, Category = "Weapon")
	void StartFire();

	UFUNCTION(BlueprintCallable, Server, Reliable, Category = "Weapon")
	void StopFire();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	void Fire();
	
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable, Category = "Weapon")
	void Reload();

	UFUNCTION(BlueprintCallable, Server, Reliable, Category = "Weapon")
	void FinishReload();

	UFUNCTION(BlueprintCallable, NetMulticast,  Reliable, Category = "Weapon")
	void Equip();

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable, Category = "Weapon")
	void Unequip();

	UFUNCTION(BlueprintCallable, BlueprintPure, BlueprintNativeEvent, Category = "Weapon")
	bool CanFire();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void AutoFireTrigger();

	UPROPERTY(BlueprintReadWrite, Category="Weapon")
	bool bIsEquipped;

	UPROPERTY(BlueprintReadWrite, Category="Weapon")
	bool bIsReloading;

	UPROPERTY(BlueprintReadWrite, Category="Weapon")
	bool bIsAutoFiring;

	UPROPERTY(BlueprintReadWrite, Category="Weapon")
	bool bIsOnCooldown;
	
	UPROPERTY(BlueprintReadWrite, Category="Weapon")
	bool bIsFireStarted;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int32 MaxAmmo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing=OnRep_CurrentAmmo, Category = "Weapon")
	int32 CurrentAmmo;

	UFUNCTION()
	void OnRep_CurrentAmmo();

	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FWeaponEvent OnAmmoChanged;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName AttachToSocketName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	EWeaponType WeaponType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float ReloadTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float EquipTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	bool bIsAutomatic;

	UPROPERTY()
	FTimerHandle ReloadTimerHandle;
	UPROPERTY()
	FTimerHandle EquipTimerHandle;
	UPROPERTY()
	FTimerHandle FireTimerHandle;
	UPROPERTY()
	FTimerHandle CooldownTimerHandle;
	
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FWeaponEvent StartReload;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
};
