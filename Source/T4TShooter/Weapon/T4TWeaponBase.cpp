// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/T4TWeaponBase.h"
#include "Net/UnrealNetwork.h"


// Sets default values
AT4TWeaponBase::AT4TWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bIsEquipped = false;
	bIsReloading = false;
	bIsOnCooldown = false;
	bIsFireStarted = false;
	bIsAutoFiring = false;
	MaxAmmo = 30;
	CurrentAmmo = MaxAmmo;
	FireRate = 0.1f;
	ReloadTime = 1.0f;
	EquipTime = 1.0f;
	bIsAutomatic = false;
}

// Called when the game starts or when spawned
void AT4TWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentAmmo = MaxAmmo;
}

// Called every frame
void AT4TWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AT4TWeaponBase::FinishReload_Implementation()
{
	bIsReloading = false;
	bIsOnCooldown = false;
	CurrentAmmo = MaxAmmo;
}

void AT4TWeaponBase::AutoFireTrigger()
{
	bIsOnCooldown = false;
	if(bIsAutoFiring && bIsFireStarted)
	{
		StartFire();
	}
}

void AT4TWeaponBase::OnRep_CurrentAmmo()
{
	OnAmmoChanged.Broadcast();
}

void AT4TWeaponBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AT4TWeaponBase, CurrentAmmo);
}

void AT4TWeaponBase::Fire_Implementation()
{
	CurrentAmmo--;
	if(CurrentAmmo<= 0)
	{
		Reload();
	}
}

void AT4TWeaponBase::Unequip_Implementation()
{
	bIsEquipped = false;
}

bool AT4TWeaponBase::CanFire_Implementation() 
{
	if(CurrentAmmo<= 0 && !bIsReloading)
	{
		Reload();
		return false;
	}
	if(bIsOnCooldown || bIsReloading || !bIsEquipped)
	{
		return false;
	}
	return true;
}

void AT4TWeaponBase::Equip_Implementation()
{
	UWorld * World = GetWorld();
	if (World)
	{
		World->GetTimerManager().SetTimer(EquipTimerHandle, FTimerDelegate::CreateLambda([&]{bIsEquipped = true;}), EquipTime, false);
	}
}

void AT4TWeaponBase::Reload_Implementation()
{

	CooldownTimerHandle.Invalidate();
	
	UWorld * World = GetWorld();
	if (World)
	{
		bIsReloading = true;
		if(GetLocalRole() == ROLE_Authority)
		{
			StopFire();
			World->GetTimerManager().SetTimer(ReloadTimerHandle, this, &AT4TWeaponBase::FinishReload, ReloadTime, false);
		}
		
		StartReload.Broadcast();
	}
}

void AT4TWeaponBase::StopFire_Implementation()
{
	bIsAutoFiring = false;
	bIsFireStarted = false;
	FireTimerHandle.Invalidate();
	
}

void AT4TWeaponBase::StartFire_Implementation()
{
	bIsFireStarted = true;
	UWorld * World = GetWorld();
	if (World)
	{
		if(CanFire())
		{
			Fire();
			bIsOnCooldown = true;
			if(!bIsAutomatic)
			{
				World->GetTimerManager().SetTimer(CooldownTimerHandle, FTimerDelegate::CreateLambda([&]{bIsOnCooldown = false;}), FireRate, false);
			}
		}
		
		if(bIsAutomatic)
		{
			bIsAutoFiring = true;
            World->GetTimerManager().SetTimer(FireTimerHandle, this, &AT4TWeaponBase::AutoFireTrigger, FireRate, false);
		}
	}
}

