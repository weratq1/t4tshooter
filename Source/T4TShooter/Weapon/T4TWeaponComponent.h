// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "T4TWeaponComponent.generated.h"



class AT4TWeaponBase;
class USkeletalMeshComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponChanged, AT4TWeaponBase*, CurrentWeapon);

UCLASS( Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class T4TSHOOTER_API UT4TWeaponComponent : public UActorComponent
{
	
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UT4TWeaponComponent();

	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Weapon")
	TArray<TSubclassOf<AT4TWeaponBase>> WeaponClassOnStart;

	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing=OnRep_CurrentWeapons, Category = "Weapon")
	TArray<AT4TWeaponBase*> CurrentWeapons;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_CurrentWeapon, Category = "Weapon")
	AT4TWeaponBase* CurrentWeapon;

	UPROPERTY(BlueprintReadWrite, Replicated, Category = "Weapon")
	USceneComponent* AttachToComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	FName CurrWeaponAttachToSocketName;

	
	
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnRep_CurrentWeapon();

	UFUNCTION(BlueprintNativeEvent, Category = "Weapon")
	void OnRep_CurrentWeapons();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	void StartFire();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	void StopFire();

	UFUNCTION(BlueprintCallable, Unreliable, Server, Category = "Weapon")
	void ChangeWeapon(int32 WeaponIndex);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Weapon")
	int GetCurrWeaponIndex();

	UFUNCTION(BlueprintCallable, Server, Reliable, Category = "Weapon")
	void InitWeaponComponent(USkeletalMeshComponent* AttachToMeshComponent);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FOnWeaponChanged OnWeaponChanged;
	
};
