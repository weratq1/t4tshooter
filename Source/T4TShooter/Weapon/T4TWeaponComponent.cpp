// Fill out your copyright notice in the Description page of Project Settings.


#include "T4TWeaponComponent.h"

#include "T4TWeaponBase.h"
#include "Net/UnrealNetwork.h"
#include "Components/SkeletalMeshComponent.h"




// Sets default values for this component's properties
UT4TWeaponComponent::UT4TWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
	// ...
}


// Called when the game starts
void UT4TWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UT4TWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UT4TWeaponComponent::OnRep_CurrentWeapons_Implementation()
{
	if(AttachToComponent)
	{
		for(auto It : CurrentWeapons)
		{
			if(It)
			{
				It->AttachToComponent(AttachToComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, It->AttachToSocketName);
			}
		}
	}
}

void UT4TWeaponComponent::StartFire_Implementation()
{
	if(CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void UT4TWeaponComponent::OnRep_CurrentWeapon_Implementation()
{
	if(IsValid(CurrentWeapon))
	{
		OnWeaponChanged.Broadcast(CurrentWeapon);
		CurrentWeapon->Equip();
	}
}

void UT4TWeaponComponent::StopFire_Implementation()
{
	if(IsValid(CurrentWeapon))
	{
		CurrentWeapon->StopFire();
	}
}

void UT4TWeaponComponent::ChangeWeapon_Implementation(int32 WeaponIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("ChangeWeapon %d"), WeaponIndex);
	if(CurrentWeapons.IsValidIndex(WeaponIndex))
	{
		if(IsValid(CurrentWeapon))
		{
			CurrentWeapon->StopFire();
			CurrentWeapon->Unequip();
		}
		CurrentWeapon = CurrentWeapons[WeaponIndex];
		OnRep_CurrentWeapon();
		CurrentWeapon->AttachToComponent(AttachToComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, CurrentWeapon->AttachToSocketName);
		CurrentWeapon->Equip();
	}
}

void UT4TWeaponComponent::InitWeaponComponent_Implementation(USkeletalMeshComponent* AttachToMeshComponent)
{
	FActorSpawnParameters Params;
    Params.Owner = GetOwner();
	for(auto It : WeaponClassOnStart)
	{
		AT4TWeaponBase* Weapon = GetWorld()->SpawnActor<AT4TWeaponBase>(It, FTransform(),Params);
		if(Weapon)
		{
			if(AttachToMeshComponent)
			{
				Weapon->AttachToComponent(AttachToMeshComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, Weapon->AttachToSocketName);
			}
			CurrentWeapons.Add(Weapon);
		}
	}
	if(CurrentWeapons.Num() > 0 )
	{
		ChangeWeapon(0);
	}
	AttachToComponent = AttachToMeshComponent;
}

int UT4TWeaponComponent::GetCurrWeaponIndex_Implementation()
{
	return CurrentWeapons.Find(CurrentWeapon);
}

void UT4TWeaponComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UT4TWeaponComponent, CurrentWeapon);
	DOREPLIFETIME(UT4TWeaponComponent, CurrentWeapons);
	DOREPLIFETIME(UT4TWeaponComponent, AttachToComponent);
	
}

