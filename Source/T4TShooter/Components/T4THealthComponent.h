// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "T4THealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FT4TOnHealthChangedSignature);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class T4TSHOOTER_API UT4THealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UT4THealthComponent();

protected:
	UFUNCTION()
	void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing=OnRep_MaxHealth, Category = "Health")
	float MaxHealth;

	UFUNCTION(BlueprintNativeEvent, Category = "Health")
	void OnRep_MaxHealth();
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_CurrentHealth, Category = "Health")
	float CurrentHealth;
	
	UFUNCTION(BlueprintNativeEvent, Category = "Health")
	void OnRep_CurrentHealth();
	
	UPROPERTY(BlueprintAssignable, Category = "Health")
	FT4TOnHealthChangedSignature OnHealthChanged;

	UPROPERTY(BlueprintAssignable , Category = "Health")
	FT4TOnHealthChangedSignature OnDie;

	UPROPERTY()
	bool bIsDead;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
		
};
