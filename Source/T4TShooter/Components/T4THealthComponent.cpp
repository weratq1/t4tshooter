// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/T4THealthComponent.h"

#include "Core/T4TPlayerState.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UT4THealthComponent::UT4THealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	MaxHealth = 100.0f;
	CurrentHealth = MaxHealth;
	bIsDead = false;
	
	
	// ...
}

void UT4THealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.0f, MaxHealth);
 	OnHealthChanged.Broadcast();
 	if(CurrentHealth <= 0.0f && !bIsDead)
 	{
 		OnDie.Broadcast();
 		bIsDead = true;
 		if(InstigatedBy)
 		{
 			AT4TPlayerState* PS = Cast<AT4TPlayerState>(InstigatedBy->PlayerState);
 			if(PS)
 			{
 				PS->AddKill();
 			}
 		}
 	}
}

// Called when the game starts
void UT4THealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	AActor* Owner = GetOwner();
	if(Owner)
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UT4THealthComponent::HandleTakeAnyDamage);
	}
}


// Called every frame
void UT4THealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UT4THealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UT4THealthComponent, CurrentHealth);
	DOREPLIFETIME(UT4THealthComponent, MaxHealth);
}

void UT4THealthComponent::OnRep_CurrentHealth_Implementation()
{
	OnHealthChanged.Broadcast();
	if(CurrentHealth <= 0.0f)
	{
		OnDie.Broadcast();
	}
}

void UT4THealthComponent::OnRep_MaxHealth_Implementation()
{
	OnHealthChanged.Broadcast();
}

