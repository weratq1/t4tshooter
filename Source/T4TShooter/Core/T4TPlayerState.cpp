// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/T4TPlayerState.h"

#include "T4TGameStateBase.h"
#include "Net/UnrealNetwork.h"




void AT4TPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AT4TPlayerState, Kills);
}

void AT4TPlayerState::AddKill_Implementation()
{
	Kills++;
 	UWorld* World = GetWorld();
 	if(World)
 	{
 		AGameStateBase* GS = World->GetGameState();
 		if(GS)
 		{
 			AT4TGameStateBase* T4TGS = Cast<AT4TGameStateBase>(GS);
 			if(T4TGS)
 			{
 				T4TGS->PlayerKill(this);
 			}
 		}
 	}
	UE_LOG(LogTemp, Warning, TEXT("Player %s has %d kills"), *GetPlayerName(), Kills);
 	ForceNetUpdate();
 }

