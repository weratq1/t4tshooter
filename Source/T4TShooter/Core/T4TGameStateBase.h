// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "T4TGameStateBase.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMatchEndDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerKillDelegate);

USTRUCT(BlueprintType)
struct FPlayerStatistic
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	APlayerState* Player;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString Name;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Kills;
};

UCLASS()
class T4TSHOOTER_API AT4TGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	// Match time in seconds 
	UPROPERTY(BlueprintReadWrite, EditAnywhere, ReplicatedUsing=OnRep_MatchTime, Category = "Match")
	int32 MatchTime;
	
	UFUNCTION(BlueprintCallable,NetMulticast, Reliable, Category = "Match")
	void PlayerKill(AT4TPlayerState* Killer);
	
	UPROPERTY(BlueprintAssignable, Category = "Match")
	FPlayerKillDelegate OnPlayerKill;

	UFUNCTION(BlueprintNativeEvent)
	void OnRep_MatchTime();

	UFUNCTION(BlueprintCallable, Reliable, NetMulticast, Category = "Match")
	void OnMatchEnd();

	UPROPERTY(BlueprintAssignable, Category = "Match")
	FMatchEndDelegate OnMatchEndDelegate;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	UFUNCTION(BlueprintCallable, Category = "Match")
	TArray<FPlayerStatistic> GetPlayerStatisticsSorted() const;

protected:
	UFUNCTION()
	void TickMatchTime();

	virtual void BeginPlay() override;
	
	UPROPERTY()
    FTimerHandle MatchTimeHandle;
};
