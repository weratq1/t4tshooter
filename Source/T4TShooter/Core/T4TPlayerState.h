// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "T4TPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class T4TSHOOTER_API AT4TPlayerState : public APlayerState
{
	GENERATED_BODY()

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
public:
	UPROPERTY(BlueprintReadWrite,Replicated, Category = "Player")
	int32 Kills;

	UFUNCTION(BlueprintCallable, Server, Reliable, Category = "Player")
	void AddKill();

	
};
