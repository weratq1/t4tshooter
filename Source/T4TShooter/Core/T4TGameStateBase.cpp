// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/T4TGameStateBase.h"

#include "Net/UnrealNetwork.h"
#include "T4TPlayerState.h"

void AT4TGameStateBase::OnMatchEnd_Implementation()
{
	OnMatchEndDelegate.Broadcast();
	MatchTimeHandle.Invalidate();
}

void AT4TGameStateBase::OnRep_MatchTime_Implementation()
{
}

void AT4TGameStateBase::PlayerKill_Implementation(AT4TPlayerState* Killer)
{

	OnPlayerKill.Broadcast();
}



void AT4TGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AT4TGameStateBase, MatchTime);
}

TArray<FPlayerStatistic> AT4TGameStateBase::GetPlayerStatisticsSorted() const
{
	TArray<FPlayerStatistic> FinalStat;
	for(auto It : PlayerArray)
	{
		AT4TPlayerState* T4TPS = Cast<AT4TPlayerState>(It);
		if(T4TPS)
		{
			FPlayerStatistic PS;
			PS.Player = T4TPS;
			PS.Name = T4TPS->GetPlayerName();
			PS.Kills = T4TPS->Kills;
			FinalStat.Add(PS);
		}
	}
	
	FinalStat.Sort([&](const FPlayerStatistic& A, const FPlayerStatistic& B)
	{
		return A.Kills > B.Kills;
	});
	return FinalStat;
}

void AT4TGameStateBase::TickMatchTime()
{
	MatchTime -= 1.f;
	if(MatchTime <= 0.f)
	{
		MatchTime = 0.f;
		OnMatchEnd();
	}
}

void AT4TGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	if(GetLocalRole()== ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(MatchTimeHandle, this, &AT4TGameStateBase::TickMatchTime, 1.f, true);
	}
}
